% love.m -- https://bitbucket.org/gtcx4230sp15/love-shack.git
%
% Submit your results at: http://j.mp/gtloveshack
%

% F == function describing the interaction (ODE's right-hand side)
F = @(t, y) [0 1 ; -1 0] * y;

% Time interval
T_range = [0 16];

% Initial conditions, Y(0)
Y0 = [0.1 -0.1];

% Solve ODE
[T, Y] = ode45 (F, T_range, Y0);

% Plot: R(t) and J(t) over time t, + reference line at y=0
R = Y(:,1);
J = Y(:,2);

figure (1);
plot (T, R, '*-', T, J, 'o:', T_range, [0 0], 'r-');
xlabel ('t');
grid on;
legend ('R(t)', 'J(t)');

% Plot: Phase diagram, R(t) vs. J(t)
figure (2);
plot (R, J, '*-');
xlabel ('R(t)');
ylabel ('J(t)');
% Add cross hairs
ax = axis;
hold on;
  plot (ax(1:2), [0 0], 'r-', [0 0], ax(3:4), 'r-');
hold off;
grid on;

% eof